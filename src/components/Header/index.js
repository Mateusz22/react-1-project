import initials from "../../assets/JD2.png";
import twitter from "../../assets/twitter_icon.png";
import facebook from "../../assets/facebook_icon.png";
import linkedin from "../../assets/linkedin_icon.png";

const Header = () => {
	return (
		<div className="main_header_wrapper_wrapper">
			<div className="main_header_inititals_wrapper col-6">
				<img
					src={initials}
					alt="Initials.img"
					id="header_initials_img"
				/>
			</div>
			<ul className="nav_list col-8">
				<li>
					<a className="link-space" href="#C_1">
						About me
					</a>
				</li>
				<li>
					<a className="link-space" href="#C_2">
						Skills
					</a>
				</li>
				<li>
					<a className="link-space" href="#C_3">
						Portfolio
					</a>
				</li>
				<li>
					<a className="link-space" href="#C_4">
						Blog
					</a>
				</li>
				<li>
					<a className="link-space" href="#C_5">
						Contact me
					</a>
				</li>
				<p className="stream">|</p>
				<div className="nav_list-img">
					<li className="list-image">
						<a href="https://twitter.com/?lang=pl" target="__blank">
							<img src={twitter} alt="twitter_icon.png" />
						</a>
					</li>
					<li className="list-image">
						<a href="https://pl-pl.facebook.com/" target="__blank">
							<img src={facebook} alt="facebook_icon.png" />
						</a>
					</li>
					<li className="list-image">
						<a href="https://pl.linkedin.com/" target="__blank">
							<img src={linkedin} alt="linkedin_icon.png" />
						</a>
					</li>
				</div>
			</ul>
		</div>
	);
};

export default Header;
