import React, { Fragment } from "react";
import github_icon from "../../assets/github_icon.png";
import external_link_icon from "../../assets/external_link_icon.png";

import circle_7 from "../../assets/kola_01.png";

const Portfolio = () => {
	return (
		<Fragment>
			<h2 id="C_3">&#47;&#47;My works</h2>
			<h3>Portfolio</h3>
			<p>
				<br />
				Ut enim ad minim veniam, quis nostrud exercitation ullamco
				laboris
				<br /> nisi ut aliquip ex ea commodo consequat. Duis aute irure
				dolor in
				<br />
				reprehenderit in voluptate velit esse cillum dolore eu fugiat
				nulla
				<br />
				pariatur.
			</p>
			<div className="portfolio-img_wrapper">
				<div className="portfolio_1">
					<img src="" alt="" />
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png" />
						<img
							src={external_link_icon}
							alt="external_link_icon.png"
						/>
					</div>
				</div>
				<div className="portfolio_2">
					<img src="" alt="" />
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png" />
						<img
							src={external_link_icon}
							alt="external_link_icon.png"
						/>
					</div>
				</div>
				<div className="portfolio_3">
					<img src="" alt="" />
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png" />
						<img
							src={external_link_icon}
							alt="external_link_icon.png"
						/>
					</div>
				</div>
				<div className="portfolio_4">
					<img src="" alt="" />
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png" />
						<img
							src={external_link_icon}
							alt="external_link_icon.png"
						/>
					</div>
				</div>
				<div className="portfolio_5">
					<img src="" alt="" />
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png" />
						<img
							src={external_link_icon}
							alt="external_link_icon.png"
						/>
					</div>
				</div>
				<div className="portfolio_6">
					<img src="" alt="" />
					<div className="icons_1">
						<img src={github_icon} alt="github_icon.png" />
						<img
							src={external_link_icon}
							alt="external_link_icon.png"
						/>
					</div>
				</div>
			</div>
			<div className="circle_7">
				<img src={circle_7} alt="kola_01.png" />
			</div>
		</Fragment>
	);
};
export default Portfolio;
