import React, { Fragment } from "react";
import circle_2 from "../../assets/kola_01.png";
import circle_3 from "../../assets/kola_02.png";

const MainArticle2 = () => {
	return (
		<Fragment>
			<div className="main_article_article_2 col-11">
				<h2>&#47;&#47; I am a freelancer</h2>
				<p>Contact me if you want to work with me</p>
				<a className="mail col-7" href="mailto: gmail.com">
					Hire me
				</a>
				<a className="download col-7" href="">
					Download CV
				</a>
			</div>
			<div className="circle_2">
				<img src={circle_2} alt="kola_01.png" />
			</div>
			<div className="circle_3">
				<img src={circle_3} alt="kola_02.png" />
			</div>
		</Fragment>
	);
};
export default MainArticle2;
