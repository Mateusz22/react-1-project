import React, { Fragment } from "react";
import blog_picture_1 from "../../assets/caucasian-man-working-shoot1.jpg";

const Blog_1 = () => {
	return (
		<Fragment>
			<h2 id="C_4">&#47;&#47;Blog posts</h2>
			<h3>Hints and tips</h3>
			<div className="blog_picture">
				<img
					src={blog_picture_1}
					alt="caucasian-man-working-shoot1.jpg"
				/>
			</div>
			<div className="wrapper_articles">
				<h2>
					&#47;&#47;Title 01<span>author. 01.09.2020</span>
				</h2>
				<h3>Secondary Title</h3>
				<p>
					<br />
					Ut enim ad minim veniam, quis nostrud exercitation ullamco
					laboris nisi ut aliquip ex ea commodo consequat. Duis aute
					irure dolor in reprehenderit in voluptate velit esse cillum
					dolore eu fugiat nulla pariatur.
				</p>
				<div>
					<a href="">Read more</a>
				</div>
			</div>
		</Fragment>
	);
};
export default Blog_1;
