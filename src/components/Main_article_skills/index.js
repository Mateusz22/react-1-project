import React, { Fragment } from "react";
import circle_4 from "../../assets/kola_03.png";

const Skills = () => {
	return (
		<Fragment>
			<div className="wrapper_skills">
				<div className="circle_4">
					<img src={circle_4} alt="kola_03.png" />
				</div>
				<div className="wrapper_skills_text_and_table">
					<h2 id="C_2">&#47;&#47;Skills</h2>
					<p>
						Duis aute irure dolor in reprehenderit in voluptate
						velit
						<br />
						esse cillum dolore eu fugiat nulla pariatur.
					</p>
					<div className="wrapper_skills_table">
						<ul>
							<li className="graph-1">
								<span className="graph_text">PHP 100&#37;</span>
							</li>
							<li className="graph-2">
								<span className="graph_text">JS 90&#37;</span>
							</li>
							<li className="graph-3">
								<span className="graph_text">HTML 90&#37;</span>
							</li>
							<li className="graph-4">
								<span className="graph_text">
									NODEJS 60&#37;
								</span>
							</li>
							<li className="graph-5">
								<span className="graph_text">CSS 90&#37;</span>
							</li>
							<li className="graph-6">
								<span className="graph_text">GO 60&#37;</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</Fragment>
	);
};
export default Skills;
