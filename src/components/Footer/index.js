import React, { Fragment } from "react";
import myPicture from "../../assets/myPicture.jpg";
import circle_9 from "../../assets/kola_03.png";
import circle_10 from "../../assets/kola_03.png";

const Footer = () => {
	return (
		<Fragment>
			<div className="circle_9">
				<img src={circle_9} alt="kola_03.png" />
			</div>
			<div className="my_contact col-9">
				<p>
					john_doe@gmail.com
					<br />+ 32 123 345 567
				</p>
			</div>
			<div className="wrapper-1">
				<h2 id="C_5">&#47;&#47;Contact me</h2>
				<p>
					<br />
					If you are willing to work with me, contact me. I can join
					your
					<br />
					conference to serve you with my engineering experience.
				</p>
				<form method="post">
					<div>
						<input
							type="text"
							name="item_1"
							placeholder="Your e-mail"
						/>
						<br />
						<br />
					</div>
					<div>
						<input
							type="text"
							name="item_2"
							placeholder="Your name"
						/>
						<br />
						<br />
					</div>
					<div>
						<textarea
							type="text"
							id="input_id_3"
							cols="30"
							rows="11"
							name="item_3"
							placeholder="How can I help you?&#10;Please, put here your&#10;message&#47;request."
						></textarea>
						<button type="submit">Send</button>
					</div>
				</form>
			</div>
			<div className="wrapper-2 col-9">
				<div>
					<img src={myPicture} alt="myPicture.jpg" />
				</div>
				<p>autor: John Doe</p>
				<p>username: &#64;johndoe</p>
				<p>description: University Graduate | Software Engineer</p>
				<p>homepage: johndoe.github.pl</p>
				<p>repository type: Open&#45;source</p>
				<p>url: github.com&#47;johndoe</p>
			</div>
			<div className="circle_10">
				<img src={circle_10} alt="kola_03.png" />
			</div>
		</Fragment>
	);
};
export default Footer;
