import React, { Fragment } from "react";
import circle_5 from "../../assets/kola_03.png";
import circle_6 from "../../assets/kola_01.png";

const Tools = () => {
	return (
		<Fragment>
			<div className="circle_5">
				<img src={circle_5} alt="kola_03.png" />
			</div>
			<h2>&#47;&#47;Tools</h2>
			<h3>My essentials</h3>
			<div className="main_article_wrapper_tools col-10">
				<div className="tools_icon_1">
					<p>
						<br />
						React
						<br />
						16.6.3
					</p>
				</div>
				<div className="tools_icon_2">
					<p>
						<br />
						Webpack
						<br />
						4.19.1
					</p>
				</div>
				<div className="tools_icon_3">
					<p>
						<br />
						Express
						<br />
						4.16.4
					</p>
				</div>
				<div className="tools_icon_4">
					<p>
						<br />
						Styled
						<br />
						Components
						<br />
						4.16.4
					</p>
				</div>
				<div className="tools_icon_5">
					<p>
						<br />
						Redux
						<br />
						4.0.1
					</p>
				</div>
				<div className="tools_icon_6">
					<p>
						<br />
						Flexbox
						<br />
						4.0.1
					</p>
				</div>
				<div className="tools_icon_7">
					<p>
						<br />
						Program
						<br />
						5.2.1
					</p>
				</div>
				<div className="tools_icon_8">
					<p>
						<br />
						Program
						<br />
						5.2.2
					</p>
				</div>
			</div>
			<div className="circle_6">
				<img src={circle_6} alt="kola_01.png" />
			</div>
		</Fragment>
	);
};
export default Tools;
