import React, { Fragment } from "react";
import easycode_button from "../../assets/easy_code_button.png";

const AboutMe = () => {
	return (
		<Fragment>
			<div className="wrapper_about_me">
				<h2>&#47;&#47;About me</h2>
				<h3>All about Techy</h3>
				<div className="wrapper_about_me_text">
					<p>
						Lorem ipsum dolor sit amet,
						<br />
						<br /> consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut
						enim ad minim veniam, quis nostrud exercitation ullamco
						laboris nisi ut aliquip ex ea commodo consequat.
						<br />
						<br /> Duis aute irure dolor in reprehenderit in
						voluptate velit esse cillum dolore eu fugiat nulla
						pariatur.
					</p>
				</div>
				<h3>My interests</h3>
				<ul className="my_list">
					<li>music</li>
					<li>kitesurfing</li>
					<li>cycling</li>
				</ul>
				<div className="wrapper_button">
					<p className="code_text">Ukończyłem kurs Easy Code</p>
					<div className="code_button">
						<img src={easycode_button} alt="easy_code_button.png" />
					</div>
				</div>
			</div>
		</Fragment>
	);
};
export default AboutMe;
