import React, { Fragment } from "react";
import myPicture from "../../assets/myPicture.jpg";
import circle_1 from "../../assets/kola_03.png";
import bitbucket from "../../assets/bit_bucket_icon.png";
import dev_icon from "../../assets/dev_icon.png";

const MainArticle = () => {
	return (
		<Fragment>
			<div className="circle_1">
				<img src={circle_1} alt="kola_03.png" />
			</div>
			<div className="picture_1 col-11">
				<img src={myPicture} alt="myPicture.jpg" />
			</div>
			<div className="main_article_article col-11">
				<h1>&#47;&#47;Hi, My name is John Doe</h1>
				<h3>Software Engineer</h3>
				<p>
					Passionate Techy and Tech Author
					<br />
					with 3 years of experience within the field.
				</p>
				<p className="subtitle_1">See my works</p>
				<div className="main_article_icons_wrapper">
					<div className="bitbucket_icon">
						<img src={bitbucket} alt="bit_bucket_icon.png" />
					</div>
					<div className="dev_icon">
						<img src={dev_icon} alt="dev_icon.png" />
					</div>
				</div>
			</div>
		</Fragment>
	);
};
export default MainArticle;
