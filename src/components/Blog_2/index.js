import React, { Fragment } from "react";
import blog_picture_2 from "../../assets/close-table-technology-notebook-open.jpg";
import circle_8 from "../../assets/kola_01.png";

const Blog_2 = () => {
	return (
		<Fragment>
			<div className="blog_picture second_img">
				<img
					src={blog_picture_2}
					alt="close-table-technology-notebook-open.jpg"
				/>
			</div>
			<div className="wrapper_articles_2">
				<h2>
					&#47;&#47;Title 01<span>author. 01.09.2020</span>
				</h2>
				<h3>Secondary Title</h3>
				<p>
					<br />
					Ut enim ad minim veniam, quis nostrud exercitation ullamco
					laboris nisi ut aliquip ex ea commodo consequat. Duis aute
					irure dolor in reprehenderit in voluptate velit esse cillum
					dolore eu fugiat nulla pariatur.
				</p>
				<div>
					<a href="">Read more</a>
				</div>
			</div>
			<div className="circle_8">
				<img src={circle_8} alt="kola_01.png" />
			</div>
		</Fragment>
	);
};
export default Blog_2;
