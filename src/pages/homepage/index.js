import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import '../../App.css';



const Header = React.lazy(() => import('../../components/Header'));
const MainArticle = React.lazy(() => import('../../components/Main_article'));
const MainArticle2 = React.lazy(() => import('../../components/Main_article_2'));
const AboutMe = React.lazy(() => import('../../components/Main_article_about_me'));
const Skills = React.lazy(() => import('../../components/Main_article_skills'));
const Tools = React.lazy(() => import('../../components/Main_article_tools'));
const Portfolio = React.lazy(() => import('../../components/Main_article_portfolio'));
const Blog_1 = React.lazy(() => import('../../components/Blog_1'));
const Blog_2 = React.lazy(() => import('../../components/Blog_2'));
const Footer = React.lazy(() => import('../../components/Footer'));


const Homepage = () => {
	return (
	  	<Suspense fallback= {<div><h2>Loading...</h2></div>}>
		<div className="container col-12">
			<div className="main_header_wrapper col-12">
				<Header/>
			</div>
			<div id="C_1" className="main_article_wrappper about_me">
				<div className="main_article">
					<MainArticle/>
					<MainArticle2/>
				</div>
			</div>
			<div className="main_article_wrapper-skills col-11">
				<AboutMe/>
				<Skills/>
			</div>
			<div className="main_article_tools tools">
			<Tools/>
			</div>
			<div className="main_article_wrapper portfolio col-11">
			<Portfolio/>
			</div>
			<div className="main_article_wrapper blog col-10">
			<Blog_1/>
			<Blog_2/>
			</div>
			<div className="main_article_wrapper contact_me col-11">
			<Footer/>
			</div>
		</div>
	</Suspense>
	);
};

export default Homepage;